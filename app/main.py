import requests
from bs4 import BeautifulSoup
import json
import time
from requests import RequestException
import random

def log(*args):
    print(time.strftime("%H:%M:%S", time.localtime()), '>', *args)

def stop():
    log('Stopping')
    exit()

#% Fake user agent for requests
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'}

#% Interval between checks
action_interval = 3 * 60 * 60

#% Interval when site does not respond
failure_interval = 60

#% Interval between requests
requests_interval = 5

#% Below this threshold, the script will wait for next batch
min_points_to_continue = 5

randomization_rate = 0.3

#% Read cookie
try:
    file = open('cookie.txt', 'r')
    cook = file.readline().strip()
    if len(cook) == 0:
        log('Please place your PHPSESSID in the cookie.txt file.')
        stop()
except FileNotFoundError:
    log('./cookie.txt file not found')
    stop()

def sleep(delay):
    global randomization_rate
    interval = int(delay * randomization_rate)
    delay += int(random.random() * (2 * interval + 1) - interval)
    delay = max(1, delay)
    log('Waiting', delay, 's.')
    time.sleep(delay)

#% Read a page
def get_soup_from_page(url):
    global cookies
    cookies = {'PHPSESSID': cook}
    try:
        sleep(requests_interval)
        r = requests.get(url, cookies=cookies, headers=headers)
    except RequestException:
        log("Can't connect to the site.")
        sleep(failure_interval)
        return get_soup_from_page(url)
    return BeautifulSoup(r.text, 'html.parser')

def refresh_profile():
    log('Refreshing profile...')
    global xsrf_token, points

    try:
        soup = get_soup_from_page('https://www.steamgifts.com')

        xsrf_token = soup.find('input', {'name': 'xsrf_token'})['value']
        points = int(soup.find('span', {'class': 'nav__points'}).text)
    except TypeError:
        log('Cant recognize your cookie value.')
        log('Please place your PHPSESSID in the cookie.txt file.')
        stop()
    
    log('Profile refreshed, points available: %dP.' % points)

def process_games(tag = ""):
    global points
    log('Proccessing games ' + tag + '...')
    
    n = 1
    while points != 0:
        log('Proccessing games from page %d...' % n)

        soup = get_soup_from_page('https://www.steamgifts.com/giveaways/search?'+ tag + 'page=' + str(n))

        try:
            gifts_list = soup.find_all(lambda tag: tag.name == 'div' and tag.get('class') == ['giveaway__row-inner-wrap'])
            
            if len(gifts_list) == 0:
                #% No gifts left to enter
                break

            for item in gifts_list:
                if points < min_points_to_continue:
                    log('Out of points.', points, 'left is below %d.' % min_points_to_continue)
                    return

                game_cost = item.find_all('span', {'class': 'giveaway__heading__thin'})

                last_div = None
                for last_div in game_cost:
                    pass
                if last_div:
                    game_cost = last_div.getText().replace('(', '').replace(')', '').replace('P', '')
                
                game_cost = int(game_cost)

                game_name = item.find('a', {'class': 'giveaway__heading__name'}).text
                givaway_code = item.find('a', {'class': 'giveaway__heading__name'})['href'].split('/')[2]
                
                if points - game_cost < 0:
                    log('Not enough points to enter:', game_name, '(cost: %dP).' % game_cost)
                    continue
                elif points - game_cost >= 0:
                    log('Entering giveaway:', game_name, '(cost: %dP).' % game_cost)
                    payload = {'xsrf_token': xsrf_token, 'do': 'entry_insert', 'code': givaway_code}
                    
                    entry = requests.post('https://www.steamgifts.com/ajax.php', data=payload, cookies=cookies, headers=headers)
                    sleep(requests_interval)
                    
                    try:
                        if json.loads(entry.text)['type'] == 'success':
                            log('Entered giveaway: ' + game_name + '.')
                    except:
                        log("Error happened when decoding json on response")
                    #% Refresh points
                    refresh_profile()
                
            n += 1
        except AttributeError as e:
            break

    log('List of games is ended.')

def get_games():
    global points
    
    #% Refresh points
    refresh_profile()
    
    if points < min_points_to_continue:
        log('Out of points.', points, 'left is below %d.' % min_points_to_continue)
        return
    
    process_games('type=wishlist&')
    
    if points < min_points_to_continue:
        log('Out of points.', points, 'left is below %d.' % min_points_to_continue)
        return
    
    process_games('type=group&')
    
    if points < min_points_to_continue:
        log('Out of points.', points, 'left is below %d.' % min_points_to_continue)
        return
    
    process_games()

if __name__ == '__main__':
    while True:
        get_games()
        sleep(action_interval)
